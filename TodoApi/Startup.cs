﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Raven.Client.Documents;
using Raven.Client.Extensions;
using System.Security.Cryptography.X509Certificates;
using TodoApi.Providers;
using FluentValidation.AspNetCore;
using TodoApi.Contracts;
using AspNet.Security.OpenIdConnect.Primitives;
using Raven.Client.ServerWide.Operations;
using Raven.Client.ServerWide;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using System.Text;

namespace TodoApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            HostingEnvironment = environment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "TODO Api", Version = "v1" });
            });

            // This will instantiate a communication channel between application and the RavenDB server instance.
            services.AddSingleton<IDocumentStore>(provider =>
            {
                const string databaseName = "TodoDb";
                var store = new DocumentStore
                {
                    Urls = new[] { "http://raven:8080" },
                    Database = databaseName,
                    Conventions =
                    {
                        IdentityPartsSeparator = "-"
                    }
                };
                store.Initialize();

                try
                {
                    store.Maintenance.Server.Send(new CreateDatabaseOperation(new DatabaseRecord(databaseName)));
                }
                catch { }

                return store;
            });

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = OpenIdConnectConstants.TokenTypes.Bearer;
            })
            .AddOAuthValidation()
            .AddOpenIdConnectServer(options =>
            {
                options.ProviderType = typeof(AuthorizationProvider);
                options.TokenEndpointPath = "/connect/token";
                options.AllowInsecureHttp = HostingEnvironment.IsDevelopment();
            });

            services.AddScoped<AuthorizationProvider>();
            services.AddScoped<IRavenProvider, RavenProvider>();
            services.AddScoped<IPasswordHasherProvider, PasswordHasherProvider>();
            services.AddScoped<ILoginProvider, LoginProvider>();
            services.AddScoped<IRefreshTokenProvider, RefreshTokenProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin();
                builder.AllowCredentials();
                builder.AllowAnyMethod();
                builder.AllowAnyHeader();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.Use(async (faq, next) => {
                if (faq.Request.Path == "/swagger/v1/swagger.json") {
                    var jsonContent = await File.ReadAllTextAsync(Path.Combine(HostingEnvironment.ContentRootPath, "Assets/swagger.json"));
                    var bytes = Encoding.UTF8.GetBytes(jsonContent);
                    await faq.Response.Body.WriteAsync(bytes, 0, bytes.Length);
                } else {
                    await next();
                }
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller}/{action}/{id?}");
            });

            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swagger, httpReq) => swagger.Host = httpReq.Host.Value);
            });
            app.UseSwaggerUI(c =>
             {
                 c.RoutePrefix = "swagger"; // serve the UI at root
                 c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");  

                 c.ShowExtensions();
             });
        }
    }
}
