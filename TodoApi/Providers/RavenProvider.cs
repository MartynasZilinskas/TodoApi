using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Raven.Client.Documents;
using TodoApi.Contracts;
using TodoApi.Models;

namespace TodoApi.Providers
{
    public class RavenProvider : IRavenProvider
    {
        public RavenProvider(IDocumentStore documentStore)
        {
            DocumentStore = documentStore;
        }

        protected IDocumentStore DocumentStore { get; private set; }

        public async Task CreateEntity<T>(T entity)
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                await session.StoreAsync(entity);
                await session.SaveChangesAsync();
            }
        }

        public async Task UpdateEntity<T>(string entityId, T entity)
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                await session.StoreAsync(entity, entityId);
                await session.SaveChangesAsync();
            }
        }

        public async Task<T> GetEntity<T>(string entityId)
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                var entity = await session.LoadAsync<T>(entityId);
                return entity;
            }
        }

        public async Task<List<T>> GetEntities<T>()
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                var entities = await session.Query<T>().ToListAsync();
                return entities;
            }
        }

        public async Task<List<T>> GetEntities<T>(Expression<Func<T, bool>> filter = null)
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                var entities = await session.Query<T>().Where(filter).ToListAsync();
                return entities;
            }
        }

        public async Task<List<string>> GetEntitiesIds<T>() where T : IModel
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                var entities = await session.Query<T>().Select(x => x.Id).ToListAsync();
                return entities;
            }
        }

        public async Task<List<string>> GetEntitiesIds<T>(Expression<Func<T, bool>> filter = null) where T : IModel
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                var entities = await session.Query<T>().Where(filter).Select(x => x.Id).ToListAsync();
                return entities;
            }
        }

        public async Task<bool> IsEntityExists(string entityId)
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                bool exists = await session.Advanced.ExistsAsync(entityId);
                return exists;
            }
        }

        public async Task DeleteEntity(string entityId)
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                session.Delete(entityId);
                await session.SaveChangesAsync();
            }
        }
        public async Task DeleteEntities(List<string> ids)
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                foreach (var entityId in ids)
                {
                    session.Delete(entityId);
                }
                await session.SaveChangesAsync();
            }
        }

        public async Task DeleteEntities<T>(Expression<Func<T, bool>> filter = null) where T : IModel
        {
            using (var session = DocumentStore.OpenAsyncSession())
            {
                var list = await session.Query<T>().Where(filter).ToListAsync();
                foreach (var entity in list)
                {
                    session.Delete(entity);
                }
                await session.SaveChangesAsync();
            }
        }
    }
}
