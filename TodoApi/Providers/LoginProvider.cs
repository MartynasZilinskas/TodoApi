using System;
using System.Threading.Tasks;
using TodoApi.Contracts;
using TodoApi.Models;

namespace TodoApi.Providers
{
    public class LoginProvider : ILoginProvider
    {
        public LoginProvider(IPasswordHasherProvider passwordHasherProvider, IRavenProvider ravenProvider)
        {
            PasswordHasherProvider = passwordHasherProvider;
            RavenProvider = ravenProvider;
        }
        protected IPasswordHasherProvider PasswordHasherProvider { get; set; }
        protected IRavenProvider RavenProvider { get; set; }

        public string GenerateId(string uniqueId) => $"login/{uniqueId}";


        public void SetPassword(LoginDetails entity, string password)
        {
            var salt = Guid.NewGuid().ToString().Replace("-", "");
            var saltedSecretHash = PasswordHasherProvider.CalculateHash(password, salt);
            entity.PasswordHash = saltedSecretHash;
            entity.PasswordSalt = salt;
        }
        public async Task<LoginDetails> GetEntity(string uniqueId)
        {
            var loginProviderId = GenerateId(uniqueId);
            var entity = await RavenProvider.GetEntity<LoginDetails>(loginProviderId);
            return entity;
        }

        public bool IsPasswordCorrect(LoginDetails entity, string password)
        {
            return PasswordHasherProvider.CheckPassword(password, entity.PasswordSalt, entity.PasswordHash);
        }
    }
}
