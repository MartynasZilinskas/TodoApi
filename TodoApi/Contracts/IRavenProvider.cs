using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Raven.Client.Documents;
using TodoApi.Models;

namespace TodoApi.Contracts
{
    public interface IRavenProvider
    {
        Task CreateEntity<T>(T entity);
        Task UpdateEntity<T>(string entityId, T entity);
        Task<T> GetEntity<T>(string entityId);
        Task<List<T>> GetEntities<T>();
        Task<List<T>> GetEntities<T>(Expression<Func<T, bool>> filter = null);
        Task<bool> IsEntityExists(string entityId);
        Task<List<string>> GetEntitiesIds<T>() where T : IModel;
        Task<List<string>> GetEntitiesIds<T>(Expression<Func<T, bool>> filter = null) where T : IModel;
        Task DeleteEntity(string entityId);
        Task DeleteEntities(List<string> ids);
        Task DeleteEntities<T>(Expression<Func<T, bool>> filter = null) where T : IModel;
    }
}
