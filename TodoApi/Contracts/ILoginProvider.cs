using System;
using System.Threading.Tasks;
using TodoApi.Models;

namespace TodoApi.Contracts
{
    public interface ILoginProvider
    {
        string GenerateId(string uniqueId);
        void SetPassword(LoginDetails entity, string password);
        Task<LoginDetails> GetEntity(string uniqueId);
        bool IsPasswordCorrect(LoginDetails entity, string password);
    }
}
