using System;
using System.Threading.Tasks;

namespace TodoApi.Contracts
{
    public interface IRefreshTokenProvider
    {
        string GenerateId(string refreshToken);
        Task CreateAsync(string userId, string refreshToken, string remoteIpAddress, string userAgent, TimeSpan refreshTokenLifetime);
    }
}
