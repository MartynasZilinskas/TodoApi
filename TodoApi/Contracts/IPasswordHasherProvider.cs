namespace TodoApi.Contracts
{
    public interface IPasswordHasherProvider
    {
        string CalculateHash(string password, string salt);
        string GetMD5(string str);
        bool CheckPassword(string password, string salt, string hash);
    }
}
