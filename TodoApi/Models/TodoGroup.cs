using System;

namespace TodoApi.Models
{
    public class TodoGroup: IModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
