using System;

namespace TodoApi.Models
{
    public interface IModel
    {
        string Id { get; set; }
    }
}
