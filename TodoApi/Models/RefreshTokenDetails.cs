using System;

namespace TodoApi.Models
{
    public class RefreshTokenDetails: IModel
    {
        public string Id { get; set; }

        public string UserId { get; set; }
        public string RefreshToken { get; set; }
        public DateTime DateExpires { get; set; }

        public string IpAddress { get; set; }
        public string UserAgent { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
