using System;
using TodoApi.Models.Dtos;
using FluentValidation;

namespace TodoApi.Models.Validators
{
    public class InputTodoDtoValidator : AbstractValidator<InputTodoDto>
    {
        public InputTodoDtoValidator()
        {
            RuleFor(x => x.Title).NotEmpty();
        }
    }
}
