using System;
using TodoApi.Models.Dtos;
using FluentValidation;

namespace TodoApi.Models.Validators
{
    public class InputUserRegistrationDtoValidator : AbstractValidator<InputUserRegistrationDto>
    {
        public InputUserRegistrationDtoValidator()
        {
            RuleFor(x => x.Email).NotEmpty().EmailAddress();
            RuleFor(x => x.Password).NotEmpty().Length(8, 255);
            RuleFor(x => x.ConfirmPassword).NotEmpty().Equal(x => x.Password);
        }
    }
}
