using System;
using TodoApi.Models.Dtos;
using FluentValidation;

namespace TodoApi.Models.Validators
{
    public class InputTodoGroupDtoValidator : AbstractValidator<InputTodoGroupDto>
    {
        public InputTodoGroupDtoValidator()
        {
            RuleFor(x => x.Title).NotEmpty();
        }
    }
}
