
using System;

namespace TodoApi.Models.Dtos
{
    public class InputUserRegistrationDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
