namespace TodoApi.Models.Dtos
{
    public class InputTodoGroupDto
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}
