namespace TodoApi.Models.Dtos
{
    public class InputTodoDto
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public bool IsDone { get; set; }

        public string ParentId { get; set; }
        public string GroupId { get; set; }
    }
}
