using System;

namespace TodoApi.Models
{
    public class Todo: IModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public bool IsDone { get; set; }

        public string ParentId { get; set; }
        public string GroupId { get; set; }

        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
