using System;

namespace TodoApi.Models
{
    public class User: IModel
    {
        public string Id { get; set; }

        public string Email { get; set; }
        public DateTime DateCreated { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
