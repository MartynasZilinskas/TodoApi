using System;

namespace TodoApi.Models
{
    public class LoginDetails: IModel
    {
        // Custom Id. e.g. "login/john@email.com"
        public string Id { get; set; }
        public string UserId { get; set; }
        public string UniqueId { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }

        public DateTime DateCreated { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
