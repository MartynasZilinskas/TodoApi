using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;
using TodoApi.Models.Dtos;
using TodoApi.Providers;
using System.Linq;
using TodoApi.Contracts;
using System;

namespace TodoApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class UsersController : Controller
    {
        public UsersController(IRavenProvider ravenProvider, ILoginProvider loginProvider)
        {
            RavenProvider = ravenProvider;
            LoginProvider = loginProvider;
        }
        protected IRavenProvider RavenProvider { get; private set; }
        protected ILoginProvider LoginProvider { get; set; }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]InputUserRegistrationDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userLoginId = LoginProvider.GenerateId(dto.Email);
            if (await RavenProvider.IsEntityExists(userLoginId))
            {
                return BadRequest($"{nameof(dto.Email)} already exists.");
            }

            var user = new User
            {
                Email = dto.Email,
                DateCreated = DateTime.UtcNow
            };

            await RavenProvider.CreateEntity(user);

            var loginDetails = new LoginDetails
            {
                Id = userLoginId,
                UniqueId = dto.Email,
                UserId = user.Id,
                DateCreated = DateTime.UtcNow
            };
            LoginProvider.SetPassword(loginDetails, dto.Password);
            await RavenProvider.CreateEntity(loginDetails);

            return Ok();
        }
    }
}
