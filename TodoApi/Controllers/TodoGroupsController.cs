using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;
using TodoApi.Models.Dtos;
using TodoApi.Providers;
using System.Linq;
using TodoApi.Contracts;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using Raven.Client.Documents.Linq;

namespace TodoApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class TodoGroupsController : Controller
    {
        public TodoGroupsController(IRavenProvider ravenDBProvider)
        {
            RavenProvider = ravenDBProvider;
        }
        protected IRavenProvider RavenProvider { get; private set; }


        [HttpGet]
        [Authorize]
        public async Task<IActionResult> List()
        {
            var identity = User.Identity;
            if (identity == null)
            {
                return Forbid();
            }

            var user = await RavenProvider.GetEntity<User>(identity.Name);
            if (user == null)
            {
                return Forbid();
            }

            var results = await RavenProvider.GetEntitiesIds<TodoGroup>();
            return Ok(results);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get([FromRoute]string id)
        {
            var identity = User.Identity;
            if (identity == null)
            {
                return Forbid();
            }

            var user = await RavenProvider.GetEntity<User>(identity.Name);
            if (user == null)
            {
                return Forbid();
            }

            var result = await RavenProvider.GetEntity<TodoGroup>(id);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create([FromBody]InputTodoGroupDto dto)
        {
            var identity = User.Identity;
            if (identity == null)
            {
                return Forbid();
            }

            var user = await RavenProvider.GetEntity<User>(identity.Name);
            if (user == null)
            {
                return Forbid();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newTodoEntity = new TodoGroup
            {
                Title = dto.Title,
                DateCreated = DateTime.UtcNow,
                CreatedBy = user.Id
            };

            await RavenProvider.CreateEntity(newTodoEntity);

            return CreatedAtAction(nameof(Create), newTodoEntity);
        }

        [HttpDelete]
        [Authorize]
        public async Task<IActionResult> Delete([FromRoute]string id)
        {
            var identity = User.Identity;
            if (identity == null)
            {
                return Forbid();
            }

            var user = await RavenProvider.GetEntity<User>(identity.Name);
            if (user == null)
            {
                return Forbid();
            }

            await RavenProvider.DeleteEntities<TodoGroup>(x => x.Id == id && x.CreatedBy == user.Id);
            await RavenProvider.DeleteEntities<Todo>(x => x.GroupId == id && x.CreatedBy == user.Id);

            return Ok();
        }
    }
}
