using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;
using TodoApi.Models.Dtos;
using TodoApi.Providers;
using System.Linq;
using TodoApi.Contracts;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using Raven.Client.Documents.Linq;

namespace TodoApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class TodosController : Controller
    {
        public TodosController(IRavenProvider ravenDBProvider)
        {
            RavenProvider = ravenDBProvider;
        }
        protected IRavenProvider RavenProvider { get; private set; }


        [HttpGet]
        [Authorize]
        public async Task<IActionResult> List([FromQuery]ListTodoQueryDto query)
        {
            var identity = User.Identity;
            if (identity == null)
            {
                return Forbid();
            }

            var user = await RavenProvider.GetEntity<User>(identity.Name);
            if (user == null)
            {
                return Forbid();
            }

            var results = await RavenProvider.GetEntities<Todo>(x => x.CreatedBy == user.Id && x.GroupId == query.GroupId);
            return Ok(results);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create([FromBody]InputTodoDto dto)
        {
            var identity = User.Identity;
            if (identity == null)
            {
                return Forbid();
            }

            var user = await RavenProvider.GetEntity<User>(identity.Name);
            if (user == null)
            {
                return Forbid();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newTodoEntity = new Todo
            {
                Title = dto.Title,
                IsDone = dto.IsDone,
                GroupId = dto.GroupId,
                ParentId = dto.ParentId,
                DateCreated = DateTime.UtcNow,
                CreatedBy = user.Id
            };

            await RavenProvider.CreateEntity(newTodoEntity);

            return CreatedAtAction(nameof(Create), newTodoEntity);
        }

        [HttpDelete]
        [Authorize]
        public async Task<IActionResult> Delete([FromRoute]string id)
        {
            var identity = User.Identity;
            if (identity == null)
            {
                return Forbid();
            }

            var user = await RavenProvider.GetEntity<User>(identity.Name);
            if (user == null)
            {
                return Forbid();
            }

            await RavenProvider.DeleteEntities<Todo>(x => x.Id == id && x.CreatedBy == user.Id);

            return Ok();
        }
    }
}
